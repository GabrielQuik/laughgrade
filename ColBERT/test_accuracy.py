import pandas as pd


def check_corect_pred(funny, predict):
    """This function checks the accuracy of the model against the prediction results file.
    
    :param url: Corresponding column values ​​from the CNN prediction result file
    :type url: funny - array with determinate humor (true or false)
                predict - Array with results of each sentences from Bert predictions
    :return: Number of good and bad predict of the CNN.
    :rtype: Integer
    """
    true = 0
    false = 0
    for i in range(len(funny)):
        if (funny[i] == "True" or funny[i]) and float(format(predict[i], '.2f')) >= 0.5:
            true += 1
        elif (funny[i] == "False" or not funny[i]) and float(format(predict[i], '.2f')) < 0.5:
            true += 1
        else:
            false += 1
    return true, false


def read_data(path):
    """This function read data from csv file.
    
    :param url: Correct path to csv file with results of CNN predictions.
    :type url: path - path to csv file
    :return: Array with results of each sentences from Bert predictions and array with determinate humor (true or false)
    :rtype: Array of string and float
    """
    df_test = pd.read_csv(path)
    is_funny = df_test.loc[:, 'Unnamed: 1']
    colbert_output = df_test.loc[:, 'pred']
    return is_funny, colbert_output


if __name__ == "__main__":
    is_funny, colbert_output = read_data('test_wynik30k.csv')
    good, bad = check_corect_pred(is_funny, colbert_output)
    accuracy = good/(good + bad) * 100
    print(f" Good predict: {good} \n Bad predict: {bad}")
    print(f"Accuracy: {accuracy}%")