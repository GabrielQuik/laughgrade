"""This script creates a network model and teaches it. The learned network model is saved to a file."""
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # 2 = INFO and WARNING messages are not printed
import nltk
from nltk.tokenize import sent_tokenize
import ssl
import pandas as pd
import numpy as np
import sklearn
from sklearn.model_selection import GroupKFold
from tqdm.notebook import tqdm
import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow import keras
from transformers import *
import gc


def return_id(str1, str2, truncation_strategy, length):
    """This function breaks the sentences into appropriate segments and gives them the correct ID and mask

        :param url: Text to be processed and truncated by "truncation_strategy" to size "length"
        :type url:  str1 - text to be converted into useful for bert
                    str2 - None
                    truncation_strategy - The kind of truncation that will be applied to the input ("longest_first")
                    length - max sentences length
                    tokanizer - tokanizer chosed from Bert ready to used tokanizer
        :return: Returns partially processed data that will be further processed
        :rtype: Array
    """
    inputs = tokenizer.encode_plus(str1, str2,
                                   add_special_tokens=True,
                                   max_length=length,
                                   truncation_strategy=truncation_strategy)
    input_ids = inputs["input_ids"]
    input_masks = [1] * len(input_ids)
    input_segments = inputs["token_type_ids"]
    padding_length = length - len(input_ids)
    padding_id = tokenizer.pad_token_id
    input_ids = input_ids + ([padding_id] * padding_length)
    input_masks = input_masks + ([0] * padding_length)
    input_segments = input_segments + ([0] * padding_length)
    return [input_ids, input_masks, input_segments]


def compute_input_arrays(df, columns, tokenizer):
    """This function read tokenizer and data, as well as defining the maximum sequence length that will be used for the input to Bert and
        preprocess the raw text data into useable Bert inputs.

    :param url: Raw Data from csv file and name of the columns that will be preprocess.
    :type url:  df - raw data from input csv file
                columns - name of the columns in data csv file
                tokanizer - tokanizer chosed from Bert ready to used tokanizer]
    :return: Useable inputs array for Bert to predict
    :rtype: Array
    """
    model_input = []
    for xx in range((MAX_SENTENCES * 3) + 3):
        model_input.append([])
    for _, row in tqdm(df[columns].iterrows()):
        i = 0
        sentences = sent_tokenize(row.text)
        for xx in range(MAX_SENTENCES):
            s = sentences[xx] if xx < len(sentences) else ''
            ids_q, masks_q, segments_q = return_id(s, None, 'longest_first', MAX_SENTENCE_LENGTH)
            model_input[i].append(ids_q)
            i += 1
            model_input[i].append(masks_q)
            i += 1
            model_input[i].append(segments_q)
            i += 1
        ids_q, masks_q, segments_q = return_id(row.text, None, 'longest_first', MAX_LENGTH)
        model_input[i].append(ids_q)
        i += 1
        model_input[i].append(masks_q)
        i += 1
        model_input[i].append(segments_q)
    for xx in range((MAX_SENTENCES * 3) + 3):
        model_input[xx] = np.asarray(model_input[xx], dtype=np.int32)
    return model_input


def create_model():
    """This function creates a network model that will later be learned and saved.

        :return: returns the model that needs to be learned
        :rtype: model
    """
    model_inputs = []
    f_inputs = []
    for i in range(MAX_SENTENCES):
        q_id = tf.keras.layers.Input((MAX_SENTENCE_LENGTH,), dtype=tf.int32)
        q_mask = tf.keras.layers.Input((MAX_SENTENCE_LENGTH,), dtype=tf.int32)
        q_atn = tf.keras.layers.Input((MAX_SENTENCE_LENGTH,), dtype=tf.int32)
        q_embedding = bert_model.bert(q_id, attention_mask=q_mask, token_type_ids=q_atn)[0]
        q = tf.keras.layers.GlobalAveragePooling1D()(q_embedding)

        hidden1 = keras.layers.Dense(32, activation="relu")(q)
        hidden2 = keras.layers.Dropout(0.3)(hidden1)
        hidden3 = keras.layers.Dense(8, activation='relu')(hidden2)

        f_inputs.append(hidden3)
        model_inputs.extend([q_id, q_mask, q_atn])

    a_id = tf.keras.layers.Input((MAX_LENGTH,), dtype=tf.int32)
    a_mask = tf.keras.layers.Input((MAX_LENGTH,), dtype=tf.int32)
    a_atn = tf.keras.layers.Input((MAX_LENGTH,), dtype=tf.int32)
    a_embedding = bert_model.bert(a_id, attention_mask=a_mask, token_type_ids=a_atn)[0]
    a = tf.keras.layers.GlobalAveragePooling1D()(a_embedding)
    print(a.shape)
    hidden1 = keras.layers.Dense(256, activation="relu")(a)
    hidden2 = keras.layers.Dropout(0.2)(hidden1)
    hidden3 = keras.layers.Dense(64, activation='relu')(hidden2)
    f_inputs.append(hidden3)
    model_inputs.extend([a_id, a_mask, a_atn])
    concat_ = keras.layers.Concatenate()(f_inputs)
    hiddenf1 = keras.layers.Dense(512, activation='relu')(concat_)
    hiddenf2 = keras.layers.Dropout(0.2)(hiddenf1)
    hiddenf3 = keras.layers.Dense(256, activation='relu')(hiddenf2)

    output = keras.layers.Dense(TARGET_COUNT, activation='sigmoid')(hiddenf3)  # softmax
    model = keras.Model(inputs=model_inputs, outputs=[output])
    gc.collect()
    return model


def print_evaluation_metrics(y_true, y_pred, label='', is_regression=True, label2=''):
    print('==================', label2)
    if is_regression:
        print('mean_absolute_error', label, ':', sklearn.metrics.mean_absolute_error(y_true, y_pred))
        print('mean_squared_error', label, ':', sklearn.metrics.mean_squared_error(y_true, y_pred))
        print('r2 score', label, ':', sklearn.metrics.r2_score(y_true, y_pred))
        return sklearn.metrics.mean_squared_error(y_true, y_pred)
    else:
        print('f1_score', label, ':', sklearn.metrics.f1_score(y_true, y_pred))
        matrix = sklearn.metrics.confusion_matrix(y_true, y_pred)
        print(matrix)
        TP, TN, FP, FN = matrix[1][1], matrix[0][0], matrix[0][1], matrix[1][0]
        Accuracy = (TP + TN) / (TP + FP + FN + TN)
        Precision = TP / (TP + FP)
        Recall = TP / (TP + FN)
        F1 = 2 * (Recall * Precision) / (Recall + Precision)
        print('Acc', Accuracy, 'Prec', Precision, 'Rec', Recall, 'F1', F1)
        return sklearn.metrics.accuracy_score(y_true, y_pred)


np.set_printoptions(suppress=True)
########################################### set training options
training_sample_count = 8000
training_epochs = 3
running_folds = 1
MAX_SENTENCE_LENGTH = 20
MAX_SENTENCES = 5
MAX_LENGTH = 100
########################################### read dataset
df = pd.read_csv('Data/dataset.csv')
df_train = pd.read_csv('Data/train.csv')
df_train = df_train[:training_sample_count * running_folds]

output_categories = list(df_train.columns[[1]])
input_categories = list(df_train.columns[[0]])

TARGET_COUNT = len(output_categories)
MODEL_TYPE = 'bert-base-uncased'
tokenizer = BertTokenizer.from_pretrained(MODEL_TYPE)

try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    pass
else:
    ssl._create_default_https_context = _create_unverified_https_context
nltk.download('punkt')

xx = 7
inputs = compute_input_arrays(df_train, input_categories, tokenizer)
outputs = np.asarray(df_train[output_categories])
config = BertConfig()  # print(config) to see settings
config.output_hidden_states = False  # Set to True to obtain hidden states
bert_model = TFBertModel.from_pretrained('bert-base-uncased', config=config)
########################################### create model
model = create_model()
model.summary()

print_evaluation_metrics([1, 0], [0.9, 0.1], '', True)
print_evaluation_metrics([1, 0], [1, 1], '', False)
min_acc = 100
min_test = []
valid_preds = []
test_preds = []
best_model = False
########################################### train model
for BS in [6]:
    LR = 1e-5
    print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
    print('LR=', LR)
    gkf = GroupKFold(n_splits=2).split(X=df_train.text, groups=df_train.text)
    for fold, (train_idx, valid_idx) in enumerate(gkf):
        if fold not in range(running_folds):
            continue
        train_inputs = [(inputs[i][train_idx])[:training_sample_count] for i in range(len(inputs))]
        train_outputs = (outputs[train_idx])[:training_sample_count]

        valid_inputs = [inputs[i][valid_idx] for i in range(len(inputs))]
        valid_outputs = outputs[valid_idx]

        print(len(train_idx), len(train_outputs))

        model = create_model()
        K.clear_session()
        optimizer = tf.keras.optimizers.Adam(learning_rate=LR)
        model.compile(loss='binary_crossentropy', optimizer=optimizer)
        print('model compiled')

        model.fit(train_inputs, train_outputs, epochs=training_epochs, batch_size=BS,
                  validation_split=0.2, verbose=1,
                  )
        valid_preds.append(model.predict(valid_inputs))
        acc = print_evaluation_metrics(np.array(valid_outputs), np.array(valid_preds[-1]), 'on #' + str(xx + 1))
        if acc < min_acc:
            print('new acc >> ', acc)
            min_acc = acc
            best_model = model

########################################### save model
saved_model_path = 'model8000'
model.save(saved_model_path)
print("model saved")
