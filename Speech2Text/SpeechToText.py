import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # 2 = INFO and WARNING messages are not printed
import speech_recognition as sr
from punctuator import Punctuator
from pydub import AudioSegment
from pydub.silence import split_on_silence
import shutil

def get_short_audio_transcription(path, r):
    with sr.AudioFile(path) as source:
        audio_listened = r.record(source)
        try:
            text = r.recognize_google(audio_listened, language="en-US")
        except sr.UnknownValueError as e:
            pass
    return text


def get_large_audio_transcription(path, r, folder_name):
    sound = AudioSegment.from_wav(path)
    chunks = split_on_silence(sound, min_silence_len=300, silence_thresh=sound.dBFS - 16.5)
    if not os.path.isdir(folder_name):
        os.mkdir(folder_name)
    whole_text = ""
    for i, audio_chunk in enumerate(chunks, start=1):
        chunk_filename = os.path.join(folder_name, f"chunk{i}.wav")
        audio_chunk.export(chunk_filename, format="wav")
        with sr.AudioFile(chunk_filename) as source:
            audio_listened = r.record(source)
            try:
                text = r.recognize_google(audio_listened, language="en-US")
                whole_text += ' ' + text
            except sr.UnknownValueError as e:
                pass
    return whole_text


def transcript(pathtofile, model_path):
    p = Punctuator(model_path)
    r = sr.Recognizer()
    folder_name = "sr-chunks"
    # file = 'Dataset/prototype_standup/stand_up.wav'
    if os.path.getsize(pathtofile) > 10000000:
        res = get_large_audio_transcription(pathtofile, r, folder_name)
        if os.path.exists(folder_name):
            shutil.rmtree(folder_name)
    else:
        res = get_short_audio_transcription(pathtofile, r)

    res = p.punctuate(res)

    print(res)
    return res
