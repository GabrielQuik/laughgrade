# Dataset Mockup
W tym folderze znajduje się kilka przykładowych plików, które można wykorzystać, do testowania projektu. Jednak nic nie stoi na przeszkodzie, aby użyć plików z innej lokalizacji.

## Dataset

Cały [Dataset](https://drive.google.com/drive/folders/1oDz0nNPax5JLPTl_OPMpBSjBNEzS3BFp?usp=sharing), z którego korzystamy, znajduje się na Google Drive.

[Dataset](https://drive.google.com/drive/folders/1oDz0nNPax5JLPTl_OPMpBSjBNEzS3BFp?usp=sharing) powinien umożliwiać ewaluację skuteczności wykrywania humorystycznych momentów z wykorzystaniem dostępnych narzędzi. W tym celu będzie on posiadał dwie formy - dźwiękową oraz tekstową.

Stworzone zostały prototypy datasetów, które mogą być wykorzystane w naszym programie. Jeden z nich stworzony manualnie z wykorzystaniem zasobów internetu, drugi natomiast na podstawie istniejącego datasetu o nazwie UR-FUNNY-V2 (https://github.com/ROC-HCI/UR-FUNNY).

<br>

### Standup

[Dataset](https://drive.google.com/drive/folders/1UXc6oBK3_2hos587h8owBrwLH66PxpAH?usp=sharing) zawierający wystąpienia standup, oparty na zebranych wymaganiach podgrup. Dataset ulega ciągłym optymalizowaniu oraz rozszerzaniu. W odpowiadającym folderze Dataset/standup (przechowywanym na dysku), znajdują się foldery z nagranymi standupami oraz folder others z przydatnymi narzędziami do datasetu. W skład narzędzi wchodzą pliki:

1. data_loader.py - ładujący przykładowy plik w formie tekstowej do nagranego standupu w folderze 1

Struktura wspomnianego pliku stand_up.json:

```
{
    "type": "Stand-up",
    "author": "Pete Holmes",
    "title": "Dirty clean",
    "data_sample":[
        {
            "id": 0,
            "transcript": "You know what happens, what the disease is called if you shake a baby? Shaken- yes. Shaken... Baby... Syndrome. That's the name of the disease. It might as well be called, 'Dad Lost It.' ",
            "is_funny": 1,
            "time" : {
                "start": "0:00",
                "end": "0:14"
            }
        },
        ...
     ]
}
```


Główna zawartość pliku widoczna jest pod kluczem "data_sample", zawiera ona 12 wartości, które przetrzymują informacje na temat kolejnych fragmentów pliku audio (stand_up.wav). Dla każdego fragmentu zawarty jest transkrypt, klucz "is_funny", który określa śmieszność danego fragmentu oraz czas rozpoczęcia i końca fragmentu. Jest to propozycja pliku umożliwiającego ewaluację skutecznosci naszego modelu, jego struktura może się zmienić.

2. cutter.py - autorski plik wycinający pliki wav na 3 sekundowe fragmenty (potrzebne do klasyfikacji emocji)


Wszystkie transkrypty standupów wykorzystywane w naszym projekcie pochodzą ze strony:
https://scrapsfromtheloft.com/2018/12/19/pete-holmes-dirty-clean-transcript/

<br>

### TED

Ten [dataset](https://drive.google.com/drive/folders/1CrzsdpZUZ1-yS6g4etTZMFclbbQxQaOe?usp=sharing) opiera się na datasecie o nazwie UR-FUNNY (github: https://github.com/ROC-HCI/UR-FUNNY.)
Zawartość można pobrać za pomocą poniższych linków:

Pliki mp4: https://www.dropbox.com/s/lg7kjx0kul3ansq/urfunny2_videos.zip?dl=1

Atrybuty plików: https://www.dropbox.com/sh/9h0pcqmqoplx9p2/AAC8yYikSBVYCSFjm3afFHQva?dl=1

Dataset zawiera wiele fragmentów nagrań wystąpień z kategorii TED Talks, w formacie mp4. Każdy fragment posiada transkrypcję oraz szczegółowe atrybuty w odzielnym pliku w formacie .pkl. Wiele zawartych tam atrybutów nie znalazło jednak zastosowania w naszym projekcie (np. atrybuty dotyczące strony wizualnej). Najbardziej interesujące nas pliki zawarte są w folderze Dataset/ted. W skład folderu Dataset/ted wchodzi kilka ponumerowanym zzipowanych plików zawierających wycięte wystąpienia TED w formie plików .wav. Poza tym znaleźć można również folder videos zawierający przykładowe pliki w formacie .mp4. W folderze other znaleźć można:

1. data_loader.py - skrypt mający na celu zaprezentować zawartość ważnych dla nas plików z atrybutami, oraz uruchomić odpowiedni dla atrybutów plik audio. Uwaga: Aby uruchomić ten skrypt, należy pobrać najpotrzebniejsze pliki z linku wyżej i umieścić je w folderze wraz ze skryptem.

2. language_sdk.pkl - plik z transkryptem oraz labelami dotyczącymi śmieszności wypowiedzianej kwestii

W przypadku gdyby ilość plików wav nie była wystarczająca, można zastosować się do poniższej instrukcji:

1. Pobrać pliki mp4 z linku wyżej
2. Umieścić wybrane pliki w folderze "videos" oraz przenieść go do folderu z projektem ted
3. Zainstalować aplikację ffmpeg

```
sudo apt update
sudo apt install ffmpeg
```
4. W folderze projektu stworzyć folder "wavs" i uruchomić poniższą instrukcję:

```
for f in videos/*.mp4; do v=$(echo $f | cut -c 8-) ; ffmpeg -i $f -ac 2 -f wav wavs/${v%%.*}.wav ; done
```

