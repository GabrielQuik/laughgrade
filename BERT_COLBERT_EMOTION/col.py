import tensorflow as tf
from tqdm.notebook import tqdm
import numpy as np
from nltk.tokenize import sent_tokenize
from transformers import BertTokenizer
import pandas as pd
import nltk


def return_id(str1, str2, truncation_strategy, length, tokenizer):
    inputs = tokenizer.encode_plus(str1, str2,
                                   add_special_tokens=True,
                                   max_length=length,
                                   truncation_strategy=truncation_strategy)
    input_ids = inputs["input_ids"]
    input_masks = [1] * len(input_ids)
    input_segments = inputs["token_type_ids"]
    padding_length = length - len(input_ids)
    padding_id = tokenizer.pad_token_id
    input_ids = input_ids + ([padding_id] * padding_length)
    input_masks = input_masks + ([0] * padding_length)
    input_segments = input_segments + ([0] * padding_length)
    return [input_ids, input_masks, input_segments]


def compute_input_arrays(df, columns, tokenizer):
    MAX_SENTENCE_LENGTH = 20
    MAX_SENTENCES = 5
    MAX_LENGTH = 100
    model_input = []
    for xx in range((MAX_SENTENCES * 3) + 3):
        model_input.append([])
    for _, row in tqdm(df[columns].iterrows()):
        i = 0
        sentences = sent_tokenize(row.text)
        for xx in range(MAX_SENTENCES):
            s = sentences[xx] if xx < len(sentences) else ''
            ids_q, masks_q, segments_q = return_id(s, None, 'longest_first', MAX_SENTENCE_LENGTH, tokenizer)
            model_input[i].append(ids_q)
            i += 1
            model_input[i].append(masks_q)
            i += 1
            model_input[i].append(segments_q)
            i += 1
        ids_q, masks_q, segments_q = return_id(row.text, None, 'longest_first', MAX_LENGTH, tokenizer)
        model_input[i].append(ids_q)
        i += 1
        model_input[i].append(masks_q)
        i += 1
        model_input[i].append(segments_q)
    for xx in range((MAX_SENTENCES * 3) + 3):
        model_input[xx] = np.asarray(model_input[xx], dtype=np.int32)
    return model_input


def predict(model_path, test_path):
    nltk.download('punkt')
    saved_test_path = "test_wynik30k.csv"
    model = tf.keras.models.load_model(model_path)
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
    df_test = pd.read_csv(test_path, error_bad_lines=False)
    test_inputs = compute_input_arrays(df_test, ['text'], tokenizer)
    test_predict = model.predict(test_inputs)
    # print(df_test, "\n", test_predict)
    df_test['pred'] = test_predict
    df_test.to_csv(saved_test_path, index=False)


if __name__ == "__main__":
    predict('colbert_model', "mulaney.csv")
