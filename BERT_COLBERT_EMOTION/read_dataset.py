import numpy as np
import random
def read():
    train_init = np.array([[0,0,0]])
    train = np.array([[0,0]])
    train_lb = np.array([[0]])
    test_init = np.array([[0,0,0]])
    test = np.array([[0,0]])
    test_lb = np.array([[0]])
    with open('train.txt','r') as file:
        lines = file.readlines()
        for line in lines:
            data = line.split(' ')
            ar = np.array([[float(data[0]),float(data[1]),int(data[2][0])]])
            train_init = np.append(train_init,ar,axis=0)
    with open('test.txt','r') as file:
        lines = file.readlines()
        for line in lines:
            data = line.split(' ')
            ar = np.array([[float(data[0]), float(data[1]), int(data[2][0])]])
            test_init = np.append(test_init,ar,axis=0)


    train_init = np.delete(train_init,0,axis=0)
    test_init = np.delete(test_init, 0,axis=0)

    random.shuffle(train_init)
    random.shuffle(test_init)
    for data in train_init:
        train = np.append(train,np.array([[data[0],data[1]]]),axis=0)
        train_lb = np.append(train_lb, np.array([[data[2]]]),axis=0)
    for data in test_init:
        test = np.append(test,np.array([[data[0],data[1]]]),axis=0)
        test_lb = np.append(test_lb, np.array([[data[2]]]),axis=0)

    print(train.shape)
    print(train_lb.shape)
    print(test.shape)
    print(test_lb.shape)

    return train,train_lb,test,test_lb

