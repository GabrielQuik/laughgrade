from keras.models import model_from_json
import numpy as np
import EmotionAnalysis.run as sea
import pandas as pd


def load(path_to_json,path_to_h5):
    # Wczytujemy z pliku JSON uprzednio zapisany model
    json_file = open(path_to_json, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)

    # Wczytujemy wagi wytrenowanego wczesniej modelu
    loaded_model.load_weights(path_to_h5)
    # print("Loaded model from disk")
    return loaded_model


def predict(Bert_result, Colbert_result,path_to_emotion_csv,path_to_json,path_to_h5):
    #sea.emotion_analyzer(path_to_record, "model.json","Emotion_Voice_Detection_Model.h5")
    df = pd.read_csv(path_to_emotion_csv)
    s = df.to_numpy()
    sum = 0
    how_many = 0
    for sample in s:
        # print(sample[1])
        how_many += 1
        sum += sample[1]

    average = sum / how_many
    multiplier = 1 + (average / 10 - 0.05)
    # predykcja z uzyciem naszego modelu i wypisanie wyniku
    model = load(path_to_json,path_to_h5)
    result = model.predict(np.array([[Bert_result, Colbert_result]]))
    # print(result[0][0]*multiplier)
    return result[0][0]*multiplier
