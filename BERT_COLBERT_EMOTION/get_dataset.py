import glob
import pandas as pd
from col import predict as colbert
import bert

def get_pos_data(save_txt,save_csv,labels,how_many,train_or_test):
    it = 0
    for i in glob.glob('ted_bert/'+train_or_test+'/pos/*.txt'):
        with open(i, 'r') as file:
            data = file.readline()
            save_txt.writelines("\"" + data + "\"")
            save_txt.write("\n")
            save_csv.writelines("\"" + data + "\"")
            save_csv.write("\n")
        labels.append(1)
        it += 1
        if how_many != 'all' and it >= how_many:
            break
    return labels

def get_neg_data(save_txt,save_csv,labels,how_many,train_or_test):
    it = 0
    for i in glob.glob('ted_bert/'+train_or_test+'/neg/*.txt'):
        with open(i, 'r') as file:
            data = file.readline()
            save_txt.writelines("\"" + data + "\"")
            save_txt.write("\n")
            save_csv.writelines("\"" + data + "\"")
            save_csv.write("\n")
        labels.append(0)
        it += 1
        if how_many != 'all' and it >= how_many:
            break
    return labels

def create_files_and_get_labels(train_or_test,how_many):
    labels = []
    with open ('ted_bert/'+train_or_test+'/all.csv','w') as save_csv:
        with open ('ted_bert/'+train_or_test+ '/all.txt','w') as save_txt:
            save_csv.writelines("\"" + "text" + "\"")
            save_csv.write("\n")
            labels = get_pos_data(save_txt,save_csv,labels,how_many,train_or_test)
            labels = get_neg_data(save_txt,save_csv,labels,how_many,train_or_test)
    return labels

def get_dataset(how_many,train_or_test):
    labels = create_files_and_get_labels(train_or_test,how_many)
    results = bert.predict('ted_bert/'+train_or_test+'/all.txt','model_3_epochs')
    colbert('colbert_model','ted_bert/'+train_or_test+'/all.csv')
    df = pd.read_csv('test_wynik30k.csv')
    with open(train_or_test+'.txt','w') as file:
        for i in range(len(results)):
            file.write(str(df.values[i][1]) + ' ' + str(results[i].numpy()[0]) +' '+ str(labels[i]) + '\n')
    # ds = []
    # for i in range(len(results)):
    #     ds.append((df.values[i][1],results[i].numpy()[0]))
    # return labels,ds

def load_dataset(train_path,test_path):
    train,trainlabels,test,testlabels = [],[],[],[]
    with open(train_path,'r') as f:
        lines = f.readlines()
        for line in lines:
            x = line.split(' ')
            train.append((float(x[0]),float(x[1])))
            trainlabels.append(int(x[2]))
    with open(test_path,'r') as f:
        lines = f.readlines()
        for line in lines:
            x = line.split(' ')
            test.append((float(x[0]),float(x[1])))
            testlabels.append(int(x[2]))
    return train,trainlabels,test,testlabels

def main(how_many_train,how_many_test):
    get_dataset(how_many_train,'train')
    get_dataset(how_many_test, 'test')
    trainds,trainlb, testds, testlb = load_dataset('train.txt','test.txt')
    print(trainlb)
    print(trainds)
    print(testlb)
    print(testds)
    return trainlb,trainds,testlb,testds

# def get_dataset(h):
#     labels = []
#     with open ('ted_bert/train/all.csv','w') as save_csv:
#         with open ('ted_bert/train/all.txt','w') as save_txt:
#             save_csv.writelines("\"" + "text" + "\"")
#             save_csv.write("\n")
#             it = 0
#             for i in glob.glob('ted_bert/train/pos/*.txt'):
#                 with open(i,'r') as file:
#                     data = file.readline()
#                     save_txt.writelines("\""+data+"\"")
#                     save_txt.write("\n")
#                     save_csv.writelines("\""+data+"\"")
#                     save_csv.write("\n")
#                 labels.append(1)
#                 it+=1
#                 if it >=h:
#                     break
#             it = 0
#             for i in glob.glob('ted_bert/train/neg/*.txt'):
#                 with open(i,'r') as file:
#                     data = file.readline()
#                     save_txt.writelines("\""+data+"\"")
#                     save_txt.write("\n")
#                     save_csv.writelines("\""+data+"\"")
#                     save_csv.write("\n")
#                 it+=1
#                 labels.append(0)
#                 if it >=h:
#                     break
#     results = bert.predict('ted_bert/train/all.txt','model_3_epochs')
#     colbert('colbert_model','ted_bert/train/all.csv')
#     df = pd.read_csv('test_wynik30k.csv')
#     ds = []
#     for i in range(2*h):
#         print(df.values[i][1])
#         print(df.values[i][1])
#         ds.append((df.values[i][1],results[i].numpy()[0]))
#     print(ds)
#     print(labels)

if __name__ == '__main__':
    # trainlb, trainds = get_dataset(50, 'train')
    # testlb, testds = get_dataset(10, 'test')
    trainlb, trainds, testlb, testds = main('all','all')