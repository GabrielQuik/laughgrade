import pandas as pd
import librosa
import numpy as np
import wave
import contextlib
from keras.models import model_from_json
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

def decode(predicted):
    """
        Decodes outputs from network into text and values
    :param predicted: outputs from network
    :type predicted: integers
    :return: labeled emotion, value
    :rtype: text, float
    """
    labels = ['female_angry', 'female_calm', 'female_fearful', 'female_happy', 'female_sad', 'male_angry', 'male_calm',
              'male_fearful', 'male_happy', 'male_sad']
    if labels[predicted[0]] == labels[0] or labels[predicted[0]] == labels[5]:
        value = 0.3
    elif labels[predicted[0]] == labels[1] or labels[predicted[0]] == labels[6]:
        value = 0.7
    elif labels[predicted[0]] == labels[2] or labels[predicted[0]] == labels[7]:
        value = 0.5
    elif labels[predicted[0]] == labels[3] or labels[predicted[0]] == labels[8]:
        value = 0.9
    elif labels[predicted[0]] == labels[4] or labels[predicted[0]] == labels[9]:
        value = 0.1
    return labels[predicted[0]], value


def load(json_model_path, h5_model_path):
    """
        Loading saved models from disk
    :param json_model_path: path to json model
    :type json_model_path: string
    :param h5_model_path: path to h5 model
    :type h5_model_path: string
    :return: model ready to use
    :rtype: keras sequential model
    """
    json_file = open(json_model_path, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)

    loaded_model.load_weights(h5_model_path)
    # print("Loaded model from disk")
    return loaded_model


def processing(path):
    """
        Preprocessing audio file before emotion recognition
    :param path: path to audio file
    :type path: string
    :return: preprocessed audio
    :rtype: numpy array
    """
    x, sample_rate = librosa.load('ea-chunks/' + path, res_type='kaiser_fast', duration=2.5, sr=22050 * 2, offset=0.5)
    sample_rate = np.array(sample_rate)
    mfccs = np.mean(librosa.feature.mfcc(y=x, sr=sample_rate, n_mfcc=13), axis=0)
    livedf2 = pd.DataFrame(data=mfccs)
    livedf2 = livedf2.stack().to_frame().T
    twodim = np.expand_dims(livedf2, axis=2)
    return twodim


def predict(path, json_model_path, h5_model_path):
    """
        Emotion recognition based on our loaded model
    :param path: path to audio file
    :type path: string
    :param json_model_path: path to json_model
    :type json_model_path: string
    :param h5_model_path: path to h5 model
    :type h5_model_path: string
    :return: label and value of recognized emotion
    :rtype: text, float
    """
    with contextlib.closing(wave.open('ea-chunks/' + path, 'r')) as f:
        frames = f.getnframes()
        rate = f.getframerate()
        duration = frames / float(rate)
    if duration < 3:
        return 0, 0
    else:
        model = load(json_model_path, h5_model_path)
        sound = processing(path)
        livepreds = model.predict(sound, batch_size=32, verbose=1)
        livepreds1 = livepreds.argmax(axis=1)
        liveabc = livepreds1.astype(int).flatten()
        label, value = decode(liveabc)
    return label, value
