import os
from pydub import AudioSegment
from pydub.utils import make_chunks


def cut(filepath):
    """
        Cutting audio file into 3s chunks for emotion recognition
    :param filepath: path to audio file
    :type filepath: string
    """
    folder = "ea-chunks"
    if not os.path.exists(folder):
        os.mkdir(folder)
    myaudio = AudioSegment.from_file(filepath)
    chunk_length_ms = 3000
    chunks = make_chunks(myaudio, chunk_length_ms)
    cwd = os.getcwd()
    os.chdir(folder)
    for i, chunk in enumerate(chunks):
        chunk_name = "{0}.wav".format(i)
        chunk.export(chunk_name, format="wav")
    os.chdir(cwd)
