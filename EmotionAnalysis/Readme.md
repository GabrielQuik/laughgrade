# Opis sieci

 W naszym projekcie użyliśmy Speach Emotion Analyzer które można [znaleźć tu](https://github.com/MITESHPUTHRANNEU/Speech-Emotion-Analyzer). Jest to konwolucyjna sieć neuronowa, która na podstawie 3s 
 fragmentu nagrania wav dokonuje predykcji płci osoby mówiącej oraz jednej z pięciu emocji: strachu, smutku, szczęście, złości, i spokoju. Nasz model był trenowany na powiększonym datasecie w stosunku 
 do oryginalnego o dataset TESS i próbowaliśmy zaimplementować więcej emocji co jednak się nie udało. Na wyjściu sieci jest zwracana emocja oraz predefiniowana przez nas jej wartość do dalszej 
 analizy w projekcie.

# Uczenie sieci 

 W celu nauczenia sieci w skrypcie "training.py" pobieramy dataset z [Dysku](https://drive.google.com/drive/folders/16n8CEfcqm--LsAVKCZl9HqpZTnVLx0hT) i zapisujemy go w folderze EmotionAnalysis w którym znajduje się skrypt.
 Model zapisze się w tym samym folderze w stworzonym przez skrypt folderze saved_models.

```
# Wczytanie plików z datasetu
mylist = os.listdir('Audio/')
```

```
model_name = 'Emotion_Voice_Detection_Model.h5'
save_dir = os.path.join(os.getcwd(), 'saved_models')

# Zapisywanie modelu i wag
if not os.path.isdir(save_dir):
    os.makedirs(save_dir)
model_path = os.path.join(save_dir, model_name)
model.save(model_path)
print('Saved trained model at %s ' % model_path)

# Zapisywanie do pliku json
model_json = model.to_json()
os.chdir(save_dir)
with open("model.json", "w") as json_file:
    json_file.write(model_json)
```
# Testowanie sieci

 Po nauczeniu sieci lub pobraniu przetrenowanego wcześniej z [dysku](https://drive.google.com/drive/folders/1ApPsT10MS6GT6az13JwYUA5Hh7Gk9evn) modelu mozna 
 przetestować na wybranych plikach .wav korzystając ze skryptu "run.py". W tym celu w folderze w którym znajduje się nasze skrypty umieszczamy testowane pliki .wav 
 w osobnym folderze (domyślna nazwa dataset).
 ```
    cutter.cut('dataset')
```
 Po uruchomieniu skryptu wyświetlą się predykcje dla pliku bądź plików w formie teskstowej
 Predykcja jest dokonywana co 3 sekundy i nie jest zwracana nazwa pliku dla którego jest wykonywana dlatego lepiej wykonywać predykcję dla jednego pliku.
