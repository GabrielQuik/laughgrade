from EmotionAnalysis import cutter
from EmotionAnalysis import predict
import shutil
import os
import warnings
warnings.filterwarnings("ignore")
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'  # ERROR
import csv


def emotion_analyzer(path, json_model_path, h5_model_path):
    """
        Recognizing emotion in each of audio file 3s chunks and saving output in .csv file
    :param path: path to audio file
    :type path: string
    :param json_model_path: path to json model
    :type json_model_path: string
    :param h5_model_path: path to h5 model
    :type h5_model_path: string
    """
    cutter.cut(path)
    folder = "ea-chunks"
    with open('emotions.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        for filename in os.listdir(folder):
            if filename.endswith(".wav"):
                label, value = predict.predict(filename, json_model_path, h5_model_path)
                writer.writerow([label, value])
    if os.path.exists(folder):
        shutil.rmtree(folder)
