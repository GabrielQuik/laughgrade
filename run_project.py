import os
from os.path import splitdrive

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'  # 2 = INFO and WARNING messages are not printed
import sys
import pandas as pd
import unzip
import ColBERT.testowanie
import EmotionAnalysis.run as ea
import Speech2Text.SpeechToText as stt
import Punchline.BERT.predict_standup as stand
from BERT_COLBERT_EMOTION.predict_ import predict as bce

path_to_file = sys.argv[1]

availabe_formats_3 = ["aac", "m4a", "mp3", "mp4", "ogg", "flv", "avi", "mpg", "wma"]
availabe_formats_4 = ["flac", "webm", "mpeg"]

if path_to_file[-4:] != ".wav":
    if path_to_file[-3:] in availabe_formats_3 or path_to_file[-4:] in availabe_formats_4:
        wav_filename = os.path.splitext(os.path.basename(path_to_file))[0] + ".wav"
        if path_to_file[-4:] in availabe_formats_4:
            os.system(f"ffmpeg -i {path_to_file} -ac 1 ./{wav_filename}")
        else:
            os.system(f"ffmpeg -i {path_to_file} -ac 1 ./{wav_filename}")
        path_to_file = f"./{wav_filename}"
    else:
        sys.exit("The file you've given has a wrong extention")

if not os.path.exists('saved_models'):
    unzip.unzip('saved_models.zip')
ea.emotion_analyzer(path_to_file, "saved_models/EmotionAnalysis/model.json",
                    "saved_models/EmotionAnalysis/Emotion_Voice_Detection_Model.h5")

if os.path.exists('./results'):
    if os.path.exists('./results/csvfile.csv'):
        os.remove('./results/csvfile.csv')

    if os.path.exists('./results/punch.txt'):
        os.remove('./results/punch.txt')

    if os.path.exists('./results/emotions.csv'):
        os.remove('./results/emotions.csv')

    if os.path.exists('./results/test_wynik.csv'):
        os.remove('./results/test_wynik.csv')
    os.rmdir('./results')

print("################################\n"
      "Transcription, with punctuation:\n"
      "################################\n"
      "<<=======================================================================================================>>")
asd = stt.transcript(path_to_file, 'saved_models/Speech2Text/model.pcr')
print("<<=======================================================================================================>>")

splited_transcript = asd.split(". ")

with open('csvfile.csv', 'w') as file:
    file.write("text\n")
    for line in splited_transcript:
        file.write("\"" + line + "\"")
        file.write('\n')
with open('punch.txt', 'w') as file:
    for line in splited_transcript:
        file.write(line)
        file.write('\n')

ColBERT.testowanie.predict("saved_models/ColBERT/colbert_model", "csvfile.csv")

stand.predict("punch.txt", "saved_models/BERT/FINAL_MODEL")

if not os.path.exists("results/"):
    os.makedirs("results/")

os.rename("./csvfile.csv", "./results/csvfile.csv")
os.rename("./punch.txt", "./results/punch.txt")
os.rename("./emotions.csv", "./results/emotions.csv")
os.rename("./test_wynik.csv", "./results/test_wynik.csv")

print("###########\n"
      "Prediction:\n"
      "###########\n"
      "<<=======================================================================================================>>")
try:
    if os.path.exists("./results/punch.txt") and os.path.exists("./results/emotions.csv") and os.path.exists(
            "./results/test_wynik.csv"):
        colbert = pd.read_csv("./results/test_wynik.csv")
        colbert = colbert.to_numpy()
        bert = stand.predict("./results/punch.txt", "saved_models/BERT/FINAL_MODEL")
        bert = bert.numpy()
        # print(bert)
        for i in range(0, colbert.shape[0]):
            wynik = bce(float(bert[i][0]), float(colbert[i][1]), './results/emotions.csv',
                        'saved_models/BERT_COLBERT_SEA/model.json',
                        'saved_models/BERT_COLBERT_SEA/model.h5')
            print(colbert[i][0] + ': ' + str(wynik * 100) + '%')
except IndexError:
  pass

print("<<=======================================================================================================>>")
print("Predykcja zakończona sukcesem. Wyniki znajdują się w folderze results")
