import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # 2 = INFO and WARNING messages are not printed
import tensorflow as tf
import tensorflow_text as text


def print_my_examples(inputs, results) :
    """
       Prints the results in a legible way
    :param inputs: sentences taken from the txt file
    :type inputs: list
    :param results: the results generated from the model
    :type results: list
    """
    result_for_printing = [f'line: {inputs[i]:<30} : score: {results[i][0]:.6f}' for i in range(len(inputs))]
    print(*result_for_printing, sep='\n')


def predict(path_to_test, path_to_model):
    """
        Generates the results from the saved model and displays them
    :param path_to_test: 
    :type path_to_test: 
    :param path_to_model: 
    :type path_to_model: 
    """
    reloaded_model = tf.saved_model.load(path_to_model)
    with open(path_to_test, 'r') as file:
        x = file.readlines()
    results = tf.sigmoid(reloaded_model(tf.constant(x)))
    print_my_examples(x, results)
    return results
