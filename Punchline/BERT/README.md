# Punchline Detection

Zostały stworzone 2 modele oparte na architekturze SmallBert binarnie klasyfikujące tekst pod względem obecnośći punchline'a. Pierwszy z nich nie bierze pod uwagi kontekstu, natomiast drugi przetwarza kontekst w postaci ostatniego zdania, zajdującego się tuż przed tym klasyfikowanym. 

## Punchline Detection Dataset
Dataset stworzony do tych modeli zawiera transkrypty wystąpień w postaci listy zdań. Nie są to teksty uzyskane za pomocą naszego narzędzia "speech to text". 
Biorąc to pod uwagę, potrzebne są dodatkowe narzędzia predykcyjne.

Dataset dla pierwszego modelu na tę chwilę zawiera jedynie wycinki wystąpień TED. Natomiast drugi model jest trenowany na większym datasecie rozszerzonym o standupowe zdania otrzymane za pomocą autorkiego skryptu transcript_scraper oraz ręcznego wyboru. Łącznie to 11700 plików txt zawierających po 2 zdania. W obu przypadkach dataset jest podzielony w proporcji 80/10/10.

## Wyniki
Pierwszy model osiąga 69% skuteczności (accuracy) na zbiorze testowym ( zawierającym zdania z wystąpień TED ).

Drugi model osiąga nieznacznie wyższą skuteczność na zbiorze testowym rozszerzonym roszerzonym o wystąpienia standup.

Oba modele, datasety, przykładowe rezultaty oraz więcej informacji można znaleźć na dysku pod adresem: https://drive.google.com/drive/folders/103e4USuJnjDUKYFvYwCHDdWWvtsS5Jzg?usp=sharing
