import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
import tensorflow_text as text
import warnings
import speech_recognition as sr
import shutil

def get_short_audio_transcription(path, r) :
    """
        Gets transcription from short audio
    :param path: path to wav file
    :type path: string
    :param r: object with an ability to transform the speech into text
    :type r: sr.Recognizer 
    :return: transcript of wav file
    :rtype: string
    """
    with sr.AudioFile(path) as source :
        audio_listened = r.record(source)
        try :
            text = r.recognize_google(audio_listened, language="en-US")
        except sr.UnknownValueError as e :
            pass
    return text


def get_large_audio_transcription(path, r, folder_name) :
    """
        Gets transcription from larger audio
    :param path: path to wav file
    :type path: string
    :param r: object with an ability to transform the speech into text
    :type r: sr.Recognizer 
    :param folder_name: folder for savig the chunks (will be deleted after getting the transcript)
    :type folder_name: string
    :return: list of sentences, transcript
    :rtype: list
    """
    sound = AudioSegment.from_wav(path)
    chunks = split_on_silence(sound, min_silence_len=300, silence_thresh=sound.dBFS - 16.5)
    if not os.path.isdir(folder_name) :
        os.mkdir(folder_name)
    whole_text = []
    for i, audio_chunk in enumerate(chunks, start=1) :
        chunk_filename = os.path.join(folder_name, f"chunk{i}.wav")
        audio_chunk.export(chunk_filename, format="wav")
        with sr.AudioFile(chunk_filename) as source :
            audio_listened = r.record(source)
            try :
                text = r.recognize_google(audio_listened, language="en-US")
                whole_text.append(text + '\n')
            except sr.UnknownValueError as e :
                pass
    return whole_text


def print_my_examples(inputs, results, console=False) :
    """
        Prints the results in a legible way
    :param inputs: list of sentences, possible punchlines
    :type inputs: list
    :param results: the results obtained from the model
    :type results: list
    :param console: whether to display the results in the console, defaults to False
    :type console: bool, optional
    """
    result_for_printing = [f'line: {inputs[i]}score: {results[i][0]:.6f}\n' for i in range(len(inputs))]
    if console :
        print(*result_for_printing, sep='\n')
    with open('results.txt', 'w') as out :
        out.writelines(result_for_printing)


def get_results(path_to_model, testfile) :
    """
        Generates the results from the saved model
    :param path_to_model: path to saved model
    :type path_to_model: string
    :param testfile: path to txt file to be tested
    :type testfile: string
    """
    reloaded_model = tf.saved_model.load(path_to_model)
    with open(testfile, 'r') as file :
        x = file.readlines( )
    results = tf.sigmoid(reloaded_model(tf.constant(x)))
    print_my_examples(x, results)


def save_text_from_speech(pathtofile, savefile) :
    """
        Saves transcript from speech in txt file
    :param pathtofile: path to wav file (existing)  
    :type pathtofile: string
    :param savefile: path to txt file 
    :type savefile: string
    """
    r = sr.Recognizer( )
    folder_name = "sr-chunks"
    if os.path.getsize(pathtofile) > 10000000 :
        res = get_large_audio_transcription(pathtofile, r, folder_name)
        if os.path.exists(folder_name) :
            shutil.rmtree(folder_name)
    else :
        res = get_short_audio_transcription(pathtofile, r)

    with open('transcript.txt', 'w') as file :
        file.writelines(res)


if __name__ == '__main__' :
    with warnings.catch_warnings( ) :
        warnings.filterwarnings("ignore", category=RuntimeWarning)
        from pydub import AudioSegment
        from pydub.silence import split_on_silence

    run = True
    while run :
        wavpath = input('\nEnter the path to the .wav file or press Enter to exit: ')
        try :
            if wavpath :
                print("\nTranslating audio speech into text . . .")
                save_text_from_speech(wavpath, 'transcript.txt')
                print("\nDone")
                print("\nPredicting punchline occurrences . . .")
                get_results('FINAL_MODEL', 'transcript.txt')
                print("\nDone")
                print("\nResults saved in: results.txt")
            else :
                run = False
        except FileNotFoundError :
            print("\nFile doesn't exist")
