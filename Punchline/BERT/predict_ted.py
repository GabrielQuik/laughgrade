import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # 2 = INFO and WARNING messages are not printed
import tensorflow as tf
import tensorflow_text as text
import pickle
import pandas as pd


os.environ['TF_XLA_FLAGS'] = '--tf_xla_enable_xla_devices'
def print_my_examples(inputs, results, sentences_num):
    """
       Prints the results in a legible way
    :param inputs: sentences taken from the txt file
    :type inputs: list
    :param results: the results generated from the model
    :type results: list
    :param senetences_num: information about number of sentences in the transcript, to print the reults properly
    :type sentences_num: int
    """

    result_for_printing = [f'setup: {inputs[i]:<30} : score: {results[i][0]:.6f}' if i < sentences_num - 1 else
                           f'punchline(?): {inputs[i]:<30} : score: {results[i][0]:.6f}'
                           for i in range(len(inputs))]
    print(*result_for_printing, sep='\n\n')


def load_pickle(pickle_file):
    """
        Loads transcipts from pickle file
    :param pickle_file: path to pickle file
    :type pickle_file: string
    :return: dictionary with every information about ted's speech
    :rtype: dict
    """
    try:
        with open(pickle_file, 'rb') as f:
            pickle_data = pickle.load(f)
    except UnicodeDecodeError as e:
        with open(pickle_file, 'rb') as f:
            pickle_data = pickle.load(f, encoding='latin1')
    except Exception as e:
        print('Unable to load data ', pickle_file, ':', e)
        raise
    return pickle_data


def predict(path_to_model, path_to_pickle):
    """
         Generates the results from the saved model and displays them
    :param path_to_model: path to saved model
    :type path_to_model: string
    :param path_to_pickle: path to pickle file with transcipts
    :type path_to_pickle: string
    """
    reloaded_model = tf.saved_model.load(path_to_model)
    examples = []
    text_pickle = load_pickle(path_to_pickle)

    df_lang = pd.DataFrame.from_dict(text_pickle, orient='index')

    indexes = [421, 859, 1702, 628, 103, 6759, 2765, 5444]
    sentences_num = []

    for ix in indexes:
        num_index = ix
        examples.append([])
        try:
            num_val = list(df_lang.axes[0]).index(num_index)
            for line in df_lang.values[num_val][1]:
                examples[-1].append(line)
            examples[-1].append(df_lang.values[num_val][0])
            sentences_num.append(len(df_lang.values[num_val][1]) + 1)
        except ValueError:
            pass

    print(examples)
    print(sentences_num)

    for i in range(len(examples)):
        label = "not funny" if i < 4 else "funny"
        print("\n######## Results for " + label + " appearance no. " + str(i) + ": ########\n")
        results = tf.sigmoid(reloaded_model(tf.constant(examples[i])))
        print_my_examples(examples[i], results, sentences_num[i])


if __name__ == '__main__':
    predict('model_3_epochs', "language_sdk.pkl")
